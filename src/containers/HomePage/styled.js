import styled from "styled-components";
import media from "../../styles/breakpoints";

export const Wrapper = styled.div`
  width: 100%;
  background-color: #081468;
  height: 100%;
  min-height: 100vh;
`;
export const WrapperChoose = styled.div`
  width: 100%;
  .button {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 30px;
  }
  ${media.smallDesktop`
  padding: 81px 0 469px 0;
  `}
  ${media.smallPhone`
  padding: 81px 20px 369px 20px;
  `}
`;
export const Content = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 81px 0 0 0;
  ${media.smallDesktop`
  padding: unset;
  `}
  ${media.smallPhone`
  padding: unset;
  `}
  h5 {
    font-style: normal;
    font-weight: 700;
    font-size: 32px;
    line-height: 42px;
    text-align: center;
    text-transform: uppercase;
    color: #000000;
    margin-bottom: 48px;
  }
  .title {
    font-style: normal;
    font-weight: 700;
    font-size: 14px;
    line-height: 24px;
    text-align: center;
    text-transform: capitalize;
    color: #404040;
  }
  .title-under {
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 24px;
    text-align: center;
    text-transform: capitalize;
    color: #404040;
  }
  .desc {
    line-height: 24px;
  }
  .img-text {
    display: flex;
    align-items: center;
    justify-content: center;
    img {
      width: 50%;
    }
  }
`;
