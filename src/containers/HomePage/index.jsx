import React from "react";

import { ROUTES } from "../../routes/constants";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, generatePath } from "react-router-dom";
import CustomButton from "../../components/Commons/Button";
import imgQuiz from "./images/buzzfeed-quiz.jpg";

import * as S from "./styled";

const HomePage = () => {
  const history = useHistory();
  return (
    <S.Wrapper>
      <div className="container">
        <S.WrapperChoose>
          <S.Content>
            <div className="img-text">
              <img src={imgQuiz} alt="" />
            </div>
          </S.Content>
          <div className="button">
            {" "}
            <CustomButton
              onClick={() => {
                history.push(ROUTES.QUESTIONPAGE);
              }}
              className="start-button"
            >
              <div className="tittle-button-return">Start Quiz</div>
            </CustomButton>
          </div>
        </S.WrapperChoose>
      </div>
    </S.Wrapper>
  );
};

export default HomePage;
