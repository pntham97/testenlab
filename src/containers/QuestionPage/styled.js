import styled from "styled-components";
import media from "../../styles/breakpoints";
import backgroud from "./images/backgroud-modal.png";

export const Wrapper = styled.div`
  width: 100%;
  background-color: #081468;
  height: auto;
  min-height: 100vh;
  .title {
    font-style: normal;
    font-weight: 700;
    font-size: 32px;
    line-height: 42px;
    text-align: center;
    text-transform: uppercase;
    color: #fff;
    h5 {
      padding-top: 30px;
      margin: 0;
    }
  }
  .question-title {
    font-style: normal;
    font-weight: 700;
    font-size: 22px;
    line-height: 24px;
    text-align: center;
    text-transform: initial;
    color: #fff;
    padding: 30px 0;
  }
`;
export const WrapperIn = styled.div`
  width: 100%;
  height: 100%;
`;
export const OptionsAnswer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  gap: 10px;
  border-radius: 8px;
  margin: 8px 0px;
`;
export const AddMutipChoiceQues = styled.div`
  margin-top: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  gap: 8px;
  flex-wrap: wrap;
  width: 100%;
`;
export const CardChoiceQues = styled.div`
  flex-basis: 90%;
  height: fit-content;
  background: #f8f8f8;
  box-sizing: border-box;
  border-radius: 16px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  transition: all 0.5s;
  padding: 10px 12px;
  &:hover {
    box-shadow: 0px 4px 8px rgba(51, 51, 51, 0.25);
    background: #f6c92b;
  }
  p {
    min-height: 65px;
    display: flex;
    align-items: center;
    font-size: 14px;
    line-height: 22px;
  }
  img {
    width: 117px;
    height: 65px;
    border-radius: 8px;
    float: left;
    margin-right: 10px;
    ${media.phone`
    margin-right: 6px;
  `}
  }
  .checked {
    width: 16px;
    height: 12px;
  }
`;
export const FormButton = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: center;
  gap: 0 20px;
  padding: 30px 0;
`;
export const WrapperModal = styled.div`
  padding: 44px 0 26px 0;
  background: url(${backgroud});
  .image-modal {
    display: flex;
    justify-content: center;
    margin-bottom: 50px;
  }
`;
export const WrapContentModal = styled.div`
  text-align: center;
  h5 {
    font-style: normal;
    font-weight: 700;
    font-size: 28px;
    line-height: 38px;
    text-align: center;
    text-transform: uppercase;
    color: #000000;
    margin-bottom: 20px;
    ${media.smallPhone`
    font-size: 20px;
    line-height: 30px;
  `}
  }
  .result {
    text-align: center;
    display: flex;
    gap: 0 16px;
    justify-content: center;
    align-items: center;
    margin-bottom: 20px;
    .title {
      font-style: normal;
      font-weight: 400;
      font-size: 22px;
      line-height: 24px;
      text-align: center;
      color: #000000;
      ${media.smallPhone`
      font-size: 16px;
      line-height: 24px;
  `}
    }
    .conttent-result {
      background: #279415;
      border-radius: 8px;
      padding: 4px 10px;
      font-style: normal;
      font-weight: 700;
      font-size: 22px;
      line-height: 24px;
      text-align: center;
      color: #ffffff;
    }
  }
`;
export const WrapButtonModal = styled.div`
  display: flex;
  gap: 0 8px;
  .button-modal {
    display: flex;
    align-items: center;
    gap: 0 8px;
    width: 100%;
  }
`;
