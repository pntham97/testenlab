import React, { useState, useEffect } from "react";

import { Text } from "../../components/Commons/Typo";
import CustomButton from "../../components/Commons/Button";
import { useHistory, generatePath } from "react-router-dom";
import { ROUTES } from "../../routes/constants";
import Modal from "../../components/Commons/Modal";
import Loading from "../../components/Loading";

import checked from "./images/check.svg";
import cup from "./images/cups.svg";

import * as S from "./styled";

let startTime = 0;
export default function QuestionPage() {
  const [check, setCheck] = useState(null);
  const [question, setQuestion] = useState();
  const [timeEnd, setTimeEnd] = useState();
  const [index, setIndex] = useState(0);
  const [result, setResult] = useState(0);
  const [complete, setComplete] = useState(false);
  const [dataItemCheck, setDataItemCheck] = useState({});
  const [dataResult, setDataResult] = useState([]);
  const history = useHistory();
  const [currentQuestion, setCurrentQuestion] = useState(
    question?.results?.[0]
  );

  const handleChecked = (item) => {
    if (check === item) {
      setCheck(null);
    } else setCheck(item);
    setDataItemCheck({
      question_id: index,
      answer_user: item,
    });
  };

  const prepareData = (data) => {
    const NUMBER_OF_QUESTION = 4;
    data.results = data.results.map((_data) => {
      const idxRandom = Math.floor(Math.random() * NUMBER_OF_QUESTION);
      let options = [..._data.incorrect_answers];
      options.splice(idxRandom, 0, _data.correct_answer);
      return {
        ..._data,
        options,
      };
    });

    return data;
  };

  const fetchQuestionData = () => {
    fetch("https://opentdb.com/api.php?amount=10")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setQuestion(prepareData(data));
      })
      .catch((error) => console.error(error));
  };

  const handleNext = (index) => {
    setIndex(index + 1);
    const currentResult = dataResult?.[index + 1];
    if (currentResult?.answer_user) {
      setCheck(currentResult?.answer_user);
    } else {
      setCheck(null);
    }
    const newArray = dataResult?.filter(
      (item) => item?.question_id !== dataItemCheck?.question_id
    );
    setDataResult([...newArray, dataItemCheck]);
  };
  const handlePrev = (index) => {
    setIndex(index - 1);
    const currentResult = dataResult?.[index - 1];
    if (currentResult?.answer_user) {
      setCheck(currentResult?.answer_user);
    }
  };

  const handleComplete = () => {
    const newRs = [...dataResult, dataItemCheck];
    setDataResult(newRs);
    const endTime = new Date();
    const timetotal = (endTime.getTime() - startTime.getTime()) / 1000;
    setTimeEnd(Math.floor(timetotal));
    const rs = newRs.reduce((acc, item) => {
      if (
        item?.answer_user ===
        question?.results[item.question_id]?.correct_answer
      ) {
        return acc + 1;
      }

      return acc;
    }, 0);

    setResult(rs);
    setComplete(true);
  };
  useEffect(() => {
    fetchQuestionData();
  }, []);

  useEffect(() => {
    if (question) {
      setCurrentQuestion(question?.results?.[index]);
      !startTime && (startTime = new Date());
    }
  }, [index, question]);

  if (!question) return <Loading />;
  const render = (item) => (
    <S.WrapperIn>
      <div className="title">
        <h5>
          Question {index + 1}/{question?.results?.length}
        </h5>
        <div className="question-title">
          {question?.results?.[index]?.question}
        </div>
      </div>
      <S.OptionsAnswer>
        <S.AddMutipChoiceQues>
          {item?.options?.map((_item, index) => (
            <S.CardChoiceQues
              key={index}
              style={{
                background: check === _item && "#f6c92b",
              }}
              onClick={() => handleChecked(_item)}
            >
              <div className="question-choice">
                <Text isBold>&#160;{_item}</Text>
              </div>
              <img
                src={checked}
                style={{
                  display: check === _item ? "block" : "none",
                }}
                className="checked"
                alt=""
              />
            </S.CardChoiceQues>
          ))}
        </S.AddMutipChoiceQues>
      </S.OptionsAnswer>
      <S.FormButton>
        <CustomButton
          onClick={() => handlePrev(index)}
          style={{ display: index !== 0 ? "flex" : "none" }}
          className="prev"
        >
          <div className="tittle-button-return">Prev</div>
        </CustomButton>
        <CustomButton
          onClick={() => handleNext(index)}
          disabled={check === null}
          style={{
            display: index !== question?.results?.length - 1 ? "flex" : "none",
          }}
          className="start-button"
        >
          <div className="tittle-button-return">Next</div>
        </CustomButton>
        <CustomButton
          style={{
            display: index === question?.results?.length - 1 ? "flex" : "none",
          }}
          disabled={check === null}
          onClick={() => handleComplete()}
          className="button-form"
        >
          <div className="tittle-button-return">Sumit</div>
        </CustomButton>
        <Modal
          title="Kết Quả"
          width="616px"
          onCancel={() => history.push(generatePath(ROUTES.HOME))}
          visible={complete}
          has_button={false}
        >
          <S.WrapperModal>
            <div className="image-modal">
              <img src={cup} alt="" />
            </div>
            <S.WrapContentModal>
              {result >= 5 ? <h5>Congratulations</h5> : <h5>Completed</h5>}
              <h5>Bạn Đã Hoàn thành phần thi trong {timeEnd}s</h5>
              <div className="result">
                <div className="title">Bạn trả lời đúng</div>
                <div className="conttent-result">
                  {result}/{question?.results?.length}
                </div>
              </div>
            </S.WrapContentModal>
            <S.WrapButtonModal>
              <CustomButton
                onClick={() => history.push(generatePath(ROUTES.CHOOSE_FIELDS))}
                className="button-modal"
              >
                Play Again
              </CustomButton>
            </S.WrapButtonModal>
          </S.WrapperModal>
        </Modal>
      </S.FormButton>
    </S.WrapperIn>
  );
  return <S.Wrapper>{render(currentQuestion)}</S.Wrapper>;
}
