import React from "react";
import CustomButton from "../Button";
import PropTypes from "prop-types";
import * as S from "./styled";

Modal.propTypes = {
  children: PropTypes.any,
  visible: PropTypes.bool,
  title: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  onCancel: PropTypes.any,
  footer: PropTypes.any,
  set_filter: PropTypes.any,
  has_button: PropTypes.bool,
  border_radius: PropTypes.string,
  widthTitle: PropTypes.string,
  margin: PropTypes.string,
  fontWeight: PropTypes.string,
  paragraph: PropTypes.string,
};

function Modal({
  title,
  titleButton,
  children,
  className,
  width = "368px",
  filter,
  set_filter,
  onCancel,
  onOk,
  onClear,
  has_button = true,
  border_radius,
  widthTitle,
  fontWeight,
  margin,
  paragraph,
  footer = null,
  ...rest
}) {
  return (
    <S.Modal
      title={title}
      width={width}
      className={className}
      visible={filter}
      onCancel={onCancel}
      onOk={onOk}
      onClear={onClear}
      footer={footer}
      $borderRadius={border_radius}
      $widthTitle={widthTitle}
      $margin={margin}
      $fontWeight={fontWeight}
      paragraph
      centered
      {...rest}
    >
      {children}
      {has_button && (
        <S.Footer>
          <CustomButton $borderRadius="8px" $width="100%" onClick={onOk}>
            {/* TODO */}
            {titleButton || "THÊM"}
          </CustomButton>
          {paragraph && (
            <S.Clear>
              <CustomButton $type="default" onClick={onClear}>
                Xóa bộ lọc
              </CustomButton>
            </S.Clear>
          )}
        </S.Footer>
      )}
    </S.Modal>
  );
}

export default Modal;
