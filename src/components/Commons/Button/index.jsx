import * as S from "./styled";

const CustomButton = ({
  children,
  size = "md",
  maxWidth,
  fontSize,
  width,
  disabledHover,
  borderRadius,
  ...rest
}) => {
  return (
    <S.ButtonCustom
      type="primary"
      $size={size}
      $maxWidth={maxWidth}
      $width={width}
      $borderRadius={borderRadius}
      {...rest}
    >
      {children}
    </S.ButtonCustom>
  );
};

export default CustomButton;
