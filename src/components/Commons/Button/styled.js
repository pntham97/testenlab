import styled from "styled-components";

export const ButtonCustom = styled.button`
  padding: 8px 16px !important;
  font-weight: 700;
  font-size: 16px;
  letter-spacing: 0.75px;
  box-sizing: border-box;
  border: 0;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  background-color: #999999;
  border-radius: ${({ $borderRadius }) => $borderRadius || "8px"};
  &:hover:enabled {
    color: #fff;
    background-color: red;
  }

  &:focus:enabled {
    color: red;
  }
`;
