export const ConvertNumberToString = (num) => {
  const mod = num % 26;
  let pow = (num / 26);
  const out = mod ? String.fromCharCode(64 + mod) : ((pow -= 1), 'Z');
  return out
};
