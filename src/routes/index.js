import React from "react";

import { BrowserRouter, Switch } from "react-router-dom";

import ScrollToTop from "../hooks/ScrollToTop";

import NonRoute from "../hocs/NonRoute";

import { ROUTES } from "./constants";
import HomeUser from "../containers/HomePage";
import QuestionPage from "../containers/QuestionPage";

const Routes = () => (
  <BrowserRouter>
    <ScrollToTop>
      <Switch>
        <NonRoute exact path={ROUTES.HOME} component={HomeUser} />
        <NonRoute exact path={ROUTES.QUESTIONPAGE} component={QuestionPage} />
      </Switch>
    </ScrollToTop>
  </BrowserRouter>
);

export default Routes;
